package id.renaldi.salt.ui

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.lifecycle.lifecycleScope
import dagger.hilt.android.AndroidEntryPoint
import id.renaldi.salt.data.local.SharedPreferenceHelper
import id.renaldi.salt.ui.auth.LoginActivity
import id.renaldi.salt.ui.main.MainActivity
import id.renaldi.salt.utility.base.BaseResponse
import id.renaldi.salt.utility.clearTask
import id.renaldi.salt.utility.newTask
import kotlinx.coroutines.delay
import javax.inject.Inject

@SuppressLint("CustomSplashScreen")
@AndroidEntryPoint
class SplashActivity: ComponentActivity() {
    @Inject
    lateinit var prefs: SharedPreferenceHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S){
            val splashScreen = installSplashScreen()
            splashScreen.setKeepOnScreenCondition { true }
        }
        super.onCreate(savedInstanceState)
        lifecycleScope.launchWhenCreated {
            delay(1500)

            if(prefs.getObject(SharedPreferenceHelper.SIGNIN_PREF, BaseResponse::class.java)!=null)
                startActivity(MainActivity.newIntent(this@SplashActivity).clearTask().newTask())
            else
                startActivity(LoginActivity.newIntent(this@SplashActivity).clearTask().newTask())

            finish()
        }
    }
}