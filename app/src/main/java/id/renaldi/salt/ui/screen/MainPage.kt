package id.renaldi.salt.ui.screen

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import id.renaldi.salt.R
import id.renaldi.salt.utility.theme.Purple700
import id.renaldi.salt.utility.theme.Teal200
import id.renaldi.salt.utility.widget.CircleImage

@Composable
fun MainPage(
    modifier: Modifier = Modifier
) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(text = stringResource(id = R.string.app_name))
                },
                backgroundColor = Purple700,
                contentColor = Color.White,
                elevation = 12.dp
            )
        },
        content = {
            Column(modifier = modifier.fillMaxWidth()) {
                Row(modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 20.dp, vertical = 10.dp)
                ){
                    CircleImage(
                        image = painterResource(id = R.drawable.ic_profile_image),
                        modifier = Modifier
                            .size(100.dp)
                    )
                }
                DescriptionWidget(
                    name = "Renaldi",
                    description = "Memiliki 5 tahun pengalaman di Android Developer. \n#JetpackCompose",
                )

                Spacer(modifier = modifier.height(20.dp))

                Row(modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 20.dp)
                ) {
                    Button(
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = Teal200
                        ),
                        onClick = { },
                        shape = RoundedCornerShape(10.dp),
                        modifier = Modifier
                            .fillMaxWidth()
                            .weight(1f)
                    ) {
                        Text(text = "Ikuti")
                    }

                    Spacer(modifier = modifier.width(20.dp))

                    Button(
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = Teal200
                        ),
                        onClick = { },
                        shape = RoundedCornerShape(10.dp),
                        modifier = Modifier
                            .fillMaxWidth()
                            .weight(1f)
                    ) {
                        Text(text = "Pesan")
                    }
                }
            }
        }
    )
}

@Composable
fun DescriptionWidget(
    name: String,
    description: String,
) {
    val letterSpacing = 0.5.sp
    val lineHeight = 20.sp
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 20.dp, vertical = 5.dp)
    ) {
        Text(
            text = name,
            fontWeight = FontWeight.Bold,
            letterSpacing = letterSpacing,
            lineHeight = lineHeight
        )
        Text(
            text = description,
            letterSpacing = letterSpacing,
            lineHeight = lineHeight
        )
    }
}