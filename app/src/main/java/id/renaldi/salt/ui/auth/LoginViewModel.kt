package id.renaldi.salt.ui.auth

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import id.renaldi.salt.data.local.SharedPreferenceHelper
import id.renaldi.salt.data.repository.AuthRepository
import id.renaldi.salt.utility.base.BaseResponse
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val repository: AuthRepository,
    private val prefs: SharedPreferenceHelper
): ViewModel() {

    private val _data = MutableLiveData<BaseResponse>()
    val data: LiveData<BaseResponse> = _data

    private val _isComplete = mutableStateOf(false)
    val isComplete: MutableState<Boolean> = _isComplete

    private val _onMessage = MutableLiveData<String>()
    val onMessage: LiveData<String> = _onMessage

    fun signin(
        username: String,
        password: String,
    ) = repository.login(username, password)
        .onStart {
            _isComplete.value = true
        }
        .onCompletion {
            _isComplete.value = false
        }
        .catch {
            _onMessage.value = it.message
        }
        .onEach {
            _data.value = it
            prefs.saveObject(SharedPreferenceHelper.SIGNIN_PREF, it)
        }.launchIn(viewModelScope)
}