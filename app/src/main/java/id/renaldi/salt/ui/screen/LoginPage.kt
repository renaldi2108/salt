package id.renaldi.salt.ui.screen

import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import id.renaldi.salt.ui.auth.LoginViewModel
import id.renaldi.salt.ui.main.MainActivity
import id.renaldi.salt.utility.clearTask
import id.renaldi.salt.utility.newTask
import id.renaldi.salt.utility.theme.AthensGray
import id.renaldi.salt.utility.theme.Teal200

@Composable
fun LoginPage(modifier: Modifier = Modifier, loginViewModel: LoginViewModel = viewModel()) {
    Column(
        modifier = modifier.padding(20.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        val context = LocalContext.current
        val focusManager = LocalFocusManager.current
        val username = remember { mutableStateOf(TextFieldValue()) }
        val password = remember { mutableStateOf(TextFieldValue()) }

        Text(text = "Login", style = TextStyle(fontSize = 40.sp))

        Spacer(modifier = modifier.height(20.dp))
        TextField(
            shape = RoundedCornerShape(10.dp),
            label = { Text(text = "Username") },
            value = username.value,
            maxLines = 1,
            onValueChange = { username.value = it },
            colors = TextFieldDefaults.textFieldColors(
                textColor = Color.Black,
                disabledTextColor = Color.Transparent,
                backgroundColor = AthensGray,
                focusedIndicatorColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent,
                disabledIndicatorColor = Color.Transparent
            ),
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Email,
                imeAction = ImeAction.Next
            ),
            keyboardActions = KeyboardActions(
                onNext = {
                    focusManager.moveFocus(FocusDirection.Down)
                }
            )
        )

        Spacer(modifier = modifier.height(20.dp))
        TextField(
            shape = RoundedCornerShape(10.dp),
            label = { Text(text = "Password") },
            value = password.value,
            maxLines = 1,
            visualTransformation = PasswordVisualTransformation(),
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Password,
                imeAction = ImeAction.Done
            ),
            onValueChange = { password.value = it },
            colors = TextFieldDefaults.textFieldColors(
                textColor = Color.Black,
                disabledTextColor = Color.Transparent,
                backgroundColor = AthensGray,
                focusedIndicatorColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent,
                disabledIndicatorColor = Color.Transparent
            )
        )

        Spacer(modifier = modifier.height(20.dp))
        Box(modifier = modifier.padding(40.dp, 0.dp, 40.dp, 0.dp)) {
            Button(
                enabled = !loginViewModel.isComplete.value,
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = Teal200
                ),
                onClick = {
                    loginViewModel.signin(username.value.text, password.value.text)
                },
                shape = RoundedCornerShape(50.dp),
                modifier = Modifier
                    .fillMaxWidth()
                    .height(50.dp)
            ) {
                if(loginViewModel.isComplete.value) CircularProgressIndicator(
                    modifier = modifier.size(size = 25.dp)
                )
                else Text(text = "Submit")
            }
        }

        if(loginViewModel.data.observeAsState().value!=null)
            context.startActivity(MainActivity.newIntent(context).clearTask().newTask())

        if(loginViewModel.onMessage.observeAsState().value != null)
            Toast.makeText(context, loginViewModel.onMessage.value, Toast.LENGTH_SHORT).show()
    }
}