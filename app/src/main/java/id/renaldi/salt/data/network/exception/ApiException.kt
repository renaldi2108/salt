package id.renaldi.salt.data.network.exception

import java.io.IOException

class ApiException(private val error: String) : IOException() {

    override val message: String
        get() = error
}