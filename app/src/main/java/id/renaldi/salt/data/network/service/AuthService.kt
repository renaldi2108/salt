package id.renaldi.salt.data.network.service

import LOGIN
import id.renaldi.salt.data.network.config.*
import id.renaldi.salt.utility.base.BaseResponse
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface AuthService {
    @FormUrlEncoded
    @POST(LOGIN)
    suspend fun authLogin(
        @Field(USERNAME) username: String,
        @Field(PASSWORD) password: String,
    ): Response<BaseResponse>
}