package id.renaldi.salt.data.local

import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import javax.inject.Inject

class SharedPreferenceHelper @Inject constructor(
    private val preference: SharedPreferences,
    private val gson: Gson
) {

    companion object {
        val SIGNIN_PREF = "signin.pref"
    }

    fun saveObject(key: String, value: Any) = putObject(key, value)

    fun <T> getObject(key: String, classOfT: Class<T>) = getObjectOrNull(key, classOfT)

    fun check(callback: (SharedPreferences) -> Boolean) = callback(preference)

    fun <T> get(callback: (SharedPreferences) -> T) = callback(preference)

    fun <T> get(callback: (SharedPreferences, Gson) -> T) = callback(preference, gson)

    fun set(callback: (SharedPreferences.Editor) -> Unit) = callback(preference.edit())

    private fun <T> getObjectOrNull(key: String, classOfT: Class<T>) = try {
        gson.fromJson(preference.getString(key, ""), classOfT)
    } catch (e: JsonSyntaxException) {
        null
    }

    private fun putObject(key: String, value: Any) =
        preference.edit().putString(key, gson.toJson(value)).apply()

    fun clear(key: String?) {
        if(key == null) preference.edit().clear().apply()
        else preference.edit().remove(key).apply()
    }

}