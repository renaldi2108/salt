package id.renaldi.salt.data.repository

import id.renaldi.salt.data.network.service.AuthService
import id.renaldi.salt.utility.base.BaseRepository
import javax.inject.Inject

class AuthRepository @Inject constructor(
    private val service: AuthService,
): BaseRepository() {
    fun login(username: String, password: String) = flowNetworkCall {
        service.authLogin(username, password)
    }
}