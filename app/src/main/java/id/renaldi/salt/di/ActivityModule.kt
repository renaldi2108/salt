package id.renaldi.salt.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.scopes.ActivityScoped
import id.renaldi.salt.data.network.service.AuthService
import id.renaldi.salt.data.repository.AuthRepository

@Module
@InstallIn(ActivityComponent::class)
object ActivityModule {
    @Provides
    @ActivityScoped
    fun provideAuthRepository(
        service: AuthService
    ): AuthRepository = AuthRepository(service)
}