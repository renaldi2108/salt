package id.renaldi.salt.utility.base

open class APIResponse(
    open val error: String,
)

open class BaseResponse(
    open val token: String,
    error: String
): APIResponse(error)