package id.renaldi.salt.utility

import android.content.Intent

fun Intent.clearTask(): Intent {
    flags = flags or (Intent.FLAG_ACTIVITY_CLEAR_TASK)
    return this
}

fun Intent.newTask(): Intent {
    flags = flags or (Intent.FLAG_ACTIVITY_NEW_TASK)
    return this
}